package net.foolz.aphasia

/**
  * This trait is just to help you write List queries and Update queries. Generally these involved a bunch of
  * optional filters for lists or potential updates on update and you end up havin to craft the query piece by piece
  * by hand.
  */
trait QueryWriting {
  /**
    * Translates a tree of QueryGroups into sql strings and gives back the data value array
    * @param groups The column comparisons
    * @param query A string builder to write the query into
    * @param existingData Optional list of data to prepend to the added data
    * @param prependWhereToQuery Whether or not to add the word 'WHERE' to the query
    * @return The data list of the query
    */
  def where(groups: QueryGroup, query: StringBuilder, existingData: Seq[Any] = Seq.empty, prependWhereToQuery: Boolean = true): Seq[Any] = {
    expandQueryGroup(groups).map({
      bits =>
        if (prependWhereToQuery) {
          query.append(" WHERE ")
        }
        query.append(bits.sql)
        existingData ++ bits.data
    }).getOrElse(existingData)
  }

  /**
    * Writes the update clause
    * @param fields  The new values to set
    * @param query   Your query
    * @param existingData Optional list of data to prepend to the added data
    * @param prependSetToQuery Whether or not to add the word 'SET' to the query
    * @return The data list of the query
    */
  def updateClause(fields: Map[String, Option[Any]], query: StringBuilder, existingData: Seq[Any] = Seq.empty, prependSetToQuery: Boolean = true): Seq[Any] = {
    if (prependSetToQuery) {
      query.append(s" SET ")
    }
    query.append(fields.toSeq.flatMap({
      case (name, Some(_)) =>
        Some(s"$name = ?")
      case _ => None
    }).mkString(", "))
    existingData ++ fields.values.flatten
  }

  def noCombination(groups: Seq[QueryGroup], query: StringBuilder, existingData: Seq[Any] = Seq.empty): Seq[Any] = {
    recurseQueryGroup(groups, "", true, true).map {
      bits =>
        query.append(" ")
        query.append(bits.sql)
        existingData ++ bits.data
    }.getOrElse(existingData)
  }

  /**
    * Recursively descend and combine all the comparisons at this level with the given operator
    * @param groups A bunch of query groups
    * @param joinOperator How all the members of this query group will be joined at the end. Like 'AND', or 'OR', ','
    * @param top True if this is the top query group, False otherwise. Top groups don't need parenthesis.
    * @param inNonCombination True if this in an non-combination branch. Order bys / limits don't need extra spaces around the join operator
    * @return An object representing the parameterized sql string with ?'s for values and the data for it; in order
    */
  private def recurseQueryGroup(groups: Seq[QueryGroup], joinOperator: String, top: Boolean, inNonCombination: Boolean = false, inOrderBy: Boolean = false): Option[QueryBits] = {
    val queryBits = groups.flatMap(g => expandQueryGroup(g, false, inNonCombination, inOrderBy))
    if (queryBits.isEmpty) {
      None
    } else {
      val join = if (inNonCombination) {
        s"$joinOperator " /* order by is joined by comma, we don't need spaces on both sides of it. it looks weird */
      } else {
        s" $joinOperator " /* all other query combinators like 'AND' need spaces on both sides */
      }
      val sql = if (top || inNonCombination) {
        queryBits.map(_.sql).mkString(join) /* top level queries don't need parens, it looks bad to wrap them */
      } else {
        s"(${queryBits.map(_.sql).mkString(join)})" /* any nested clause gets parens */
      }

      Some(QueryBits(sql, queryBits.flatMap(_.data)))
    }
  }

  /**
    * Recursively descends a QueryGroup tree and builds the SQL String
    * @param queryGroup The group to descend
    * @return Some(sql, data) when built, None when no query elements are needed
    */
  private def expandQueryGroup(queryGroup: QueryGroup, top: Boolean = true, inNonCombination: Boolean = false, inOrderBy: Boolean = false): Option[QueryBits] = {
    queryGroup match {
      case whereGroup: WhereClause =>
        recurseQueryGroup(whereGroup.values, whereGroup.operator, top)
      case columnComparison: ColumnComparison =>
        /** Leaf case: Write the actual SQL to compare a column */
        columnComparison match {
          case InCol(columnName, value) if value.nonEmpty =>
            Some(QueryBits(s"$columnName IN ${List.fill(value.size)("?").mkString("(", ",", ")")}", value))
          case Cols(columnName, operator, Some(value)) =>
            Some(QueryBits(s"$columnName $operator ?", Seq(value)))
          case Between(columnName, Some((v1, v2))) =>
            Some(QueryBits(s"$columnName BETWEEN ? AND ?", Seq(v1, v2)))
          case _ => None
        }
      case OrderBy(column, direction) =>
        val base = new StringBuilder()
        if (!inOrderBy) {
          base.append(s"ORDER BY ")
        }
        Some(QueryBits(s"$base$column $direction"))
      case OrderBys(columns) =>
        recurseQueryGroup(columns, OrderBys.operator, top, inNonCombination, true).map(qb => QueryBits(s"ORDER BY ${qb.sql}"))
      case Limit(count) =>
        Some(QueryBits(s"LIMIT $count"))
      case Comment(comment) =>
        Some(QueryBits(s"-- $comment"))
      case Empty =>
        /** Skip this group */
        None
    }
  }
}

object QueryWriting extends QueryWriting


sealed abstract class Direction(val name: String) {
  override def toString: String = name
}
object Direction {
  case object Asc extends Direction("ASC")
  case object Desc extends Direction("DESC")
}


case class QueryBits(sql: String, data: Seq[Any] = Seq.empty)
