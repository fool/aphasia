package net.foolz.aphasia.paging

import net.foolz.aphasia._

trait Token[T] {
  def value: T
  def operator: Operator
}

sealed abstract class Operator(value: String) {
  override def toString: String = value
}
case object GreaterThan extends Operator(">")
case object GreaterThanEqual extends Operator(">=")
case object LessThan extends Operator("<")
case object LessThanEqual extends Operator("<=")

trait PagingQueries {
  def page(column: String, pageToken: Option[Token[_]]): QueryGroup = {
    pageToken.map {
      token =>
        Cols(column, token.operator.toString, Some(token.value))
    }.getOrElse(Empty)
  }

  def orderByPageToken(query: StringBuilder, sortColumn: String, pageToken: Option[Token[_]]): QueryGroup = {
    pageToken match {
      case Some(token) if token.operator == LessThan || token.operator == LessThanEqual => OrderBy(sortColumn, Direction.Desc)
      case Some(_) => OrderBy(sortColumn, Direction.Asc)
      case _ => Empty
    }
  }

  /** All queries using the LessThan operator (sort DESC) need to be reverse sorted in application code after */
  def reverseIfLessThan[R](results: Seq[R], pageToken: Option[Token[_]]): Seq[R] = {
    pageToken match {
      case Some(token) if token.operator == LessThan || token.operator == LessThanEqual => results.reverse
      case _ => results
    }
  }
}

object PagingQueries extends PagingQueries
