package net.foolz.aphasia

sealed trait QueryGroup

case class Limit(count: Int) extends QueryGroup
case class Comment(comment: String) extends QueryGroup

/**
  * A query can only have ONE OrderBy QueryGroup. If you need multi-ordering use OrderBys
  */
case class OrderBy(column: String, direction: Direction) extends QueryGroup
case class OrderBys(columns: Seq[OrderBy]) extends QueryGroup
object OrderBys {
  def operator = ","
}

class WhereClause(val values: Seq[QueryGroup], val operator: String) extends QueryGroup
case class Or(override val values: Seq[QueryGroup]) extends WhereClause(values, "OR")
case class And(override val values: Seq[QueryGroup]) extends WhereClause(values, "AND")
case object Empty extends QueryGroup
/**
  * Basic column comparator
  * -> Can be a multi-value operator: `ColName` IN (value list)
  * -> Or a single value operator:    `ColName` = (value)
  */
sealed trait ColumnComparison extends QueryGroup {
  def column: String
}

/** Multi-Value IN Operator
  * @param column Column name
  * @param value  value, if empty this comparison will not be included in the query
  */
case class InCol(column: String, value: Seq[Any]) extends ColumnComparison

/** Single value operator
  * @param column    Column name
  * @param operator sql operator
  * @param value    value. if Empty this comparison will not be included in the query
  */
case class Cols(column: String, operator: String, value: Option[Any]) extends ColumnComparison

/**
  * Two value operator. BETWEEN value._1 AND value._2
  * @param column Column name
  * @param value  A tuple where first item is first argument to BETWEEN and 2nd item is 2nd argument
  */
case class Between(column: String, value: Option[(Any, Any)]) extends ColumnComparison
