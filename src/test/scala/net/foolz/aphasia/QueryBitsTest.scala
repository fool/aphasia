package net.foolz.aphasia

import java.time.Instant

import org.scalatest.Assertion
import org.scalatest.funsuite.AnyFunSuite


class QueryBitsTest extends AnyFunSuite {
  val Id = "id"
  val Name = "name"
  val Date = "date"
  val Address = "address"

  test(s"where() with some values set and some none") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data = QueryWriting.where(
      And(Seq(
        Cols("Address", "=", Some("The Moon")),
        Cols("Name", "!=", None)
      )),
      queryBuilder
    )
    val query = queryBuilder.toString()

    val expectedQuery = "SELECT * FROM MY_TABLE WHERE Address = ?"
    val expectedData = Seq("The Moon")
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data)
  }

  test(s"where() with between") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data = QueryWriting.where(
      And(Seq(
        Between("Created", Some(("2021-09-11 09:00:00", "2021-09-11 11:53:00")))
      )),
      queryBuilder
    )
    val query = queryBuilder.toString()
    val expectedQuery = "SELECT * FROM MY_TABLE WHERE Created BETWEEN ? AND ?"
    val expectedData = Seq("2021-09-11 09:00:00", "2021-09-11 11:53:00")
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data)
  }

  test(s"where() with IN list") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data = QueryWriting.where(
      And(Seq(
        InCol("Id", Seq(1, 2, 3))
      )),
      queryBuilder
    )
    val query = queryBuilder.toString()
    val expectedQuery = "SELECT * FROM MY_TABLE WHERE Id IN (?,?,?)"
    val expectedData = Seq(1, 2, 3)
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data)
  }

  test(s"comment test") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data = QueryWriting.noCombination(Seq(Comment("this is a test")), queryBuilder)
    val query = queryBuilder.toString()

    val expectedQuery = "SELECT * FROM MY_TABLE -- this is a test"
    val expectedData = Seq.empty
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data)
  }

  test(s"where() with multiple values set combined with AND") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data = QueryWriting.where(
      And(Seq(
        Cols("Address", "=", Some("The Moon")),
        Cols("Name", "!=", Some("Buzz Aldrin"))
        )),
      queryBuilder
    )
    val query = queryBuilder.toString()

    val expectedQuery = "SELECT * FROM MY_TABLE WHERE Address = ? AND Name != ?"
    val expectedData = Seq("The Moon", "Buzz Aldrin")
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data)
  }

  test(s"where() with multiple types of combinations") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data = QueryWriting.where(
      Or(Seq(
        And(Seq(
        Cols("Address", "=", Some("The Moon")),
        Cols("Name", "!=", Some("Buzz Aldrin"))
        )),
        And(Seq(
          Cols("Date", "<", Some(Instant.parse("2020-06-01T00:00:00Z")))
        ))
        )),
      queryBuilder
    )
    val query = queryBuilder.toString()

    val expectedQuery = "SELECT * FROM MY_TABLE WHERE (Address = ? AND Name != ?) OR (Date < ?)"
    val expectedData = Seq("The Moon", "Buzz Aldrin", Instant.parse("2020-06-01T00:00:00Z"))
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data)
  }

  test(s"where() with multiple types of combinations with order by") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data1 = QueryWriting.where(
      Or(Seq(
        And(Seq(
          Cols("Address", "=", Some("The Moon")),
          Cols("Name", "!=", Some("Buzz Aldrin"))
        )),
        And(Seq(
          Cols("Date", "<", Some(Instant.parse("2020-06-01T00:00:00Z")))
        ))
      )),
      queryBuilder
    )
    val data2 = QueryWriting.noCombination(Seq(OrderBy(s"Id", Direction.Asc)), queryBuilder, data1)
    val query = queryBuilder.toString()

    val expectedQuery = "SELECT * FROM MY_TABLE WHERE (Address = ? AND Name != ?) OR (Date < ?) ORDER BY Id ASC"
    val expectedData = Seq("The Moon", "Buzz Aldrin", Instant.parse("2020-06-01T00:00:00Z"))
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data2)
  }

  test(s"where() with multiple types of combinations with multiple order by") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data1 = QueryWriting.where(
      Or(Seq(
        And(Seq(
          Cols("Address", "=", Some("The Moon")),
          Cols("Name", "!=", Some("Buzz Aldrin"))
        )),
        And(Seq(
          Cols("Date", "<", Some(Instant.parse("2020-06-01T00:00:00Z")))
        ))
      )),
      queryBuilder
    )
    val data2 = QueryWriting.noCombination(Seq(OrderBys(Seq(
      OrderBy(s"Id", Direction.Asc),
      OrderBy(s"Address", Direction.Desc)
      ))), queryBuilder, data1)
    val query = queryBuilder.toString()

    val expectedQuery = "SELECT * FROM MY_TABLE WHERE (Address = ? AND Name != ?) OR (Date < ?) ORDER BY Id ASC, Address DESC"
    val expectedData = Seq("The Moon", "Buzz Aldrin", Instant.parse("2020-06-01T00:00:00Z"))
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data2)
  }

  test(s"where() with multiple types of combinations with multiple order by and limit") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data1 = QueryWriting.where(
      Or(Seq(
        And(Seq(
          Cols("Address", "=", Some("The Moon")),
          Cols("Name", "!=", Some("Buzz Aldrin"))
        )),
        And(Seq(
          Cols("Date", "<", Some(Instant.parse("2020-06-01T00:00:00Z")))
        ))
      )),
      queryBuilder
    )
    val data2 = QueryWriting.noCombination(Seq(
      OrderBys(Seq(
        OrderBy(s"Id", Direction.Asc),
        OrderBy(s"Address", Direction.Desc)
        )),
      Limit(100)
    ), queryBuilder, data1)
    val query = queryBuilder.toString()

    val expectedQuery = "SELECT * FROM MY_TABLE WHERE (Address = ? AND Name != ?) OR (Date < ?) ORDER BY Id ASC, Address DESC LIMIT 100"
    val expectedData = Seq("The Moon", "Buzz Aldrin", Instant.parse("2020-06-01T00:00:00Z"))
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data2)
  }

  test("update() with multiple fields, all set") {
    val queryBuilder = new StringBuilder("UPDATE MY_TABLE")
    val data = QueryWriting.updateClause(
      Map(
        "Address" -> Some("The Moon"),
        "Name" -> Some("Buzz Aldrin")
      ),
      queryBuilder
    )
    val query = queryBuilder.toString()

    val expectedQuery = "UPDATE MY_TABLE SET Address = ?, Name = ?"
    val expectedData = Seq("The Moon", "Buzz Aldrin")
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data)
  }

  test("update() with multiple fields, some set, some missing") {
    val queryBuilder = new StringBuilder("UPDATE MY_TABLE")
    val data = QueryWriting.updateClause(
      Map(
        "Address" -> Some("The Moon"),
        "Date" -> None,
        "Name" -> Some("Buzz Aldrin")
      ),
      queryBuilder
    )
    val query = queryBuilder.toString()

    val expectedQuery = "UPDATE MY_TABLE SET Address = ?, Name = ?"
    val expectedData = Seq("The Moon", "Buzz Aldrin")
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data)
  }


  test(s"where() with empty") {
    val queryBuilder = new StringBuilder("SELECT * FROM MY_TABLE")
    val data = QueryWriting.where(
      Empty,
      queryBuilder
    )
    val query = queryBuilder.toString()

    val expectedQuery = "SELECT * FROM MY_TABLE"
    val expectedData = Seq()
    assertQueryEqual(expectedQuery, query)
    assertResult(expectedData)(data)
  }

  private def assertQueryEqual(expectedQuery: String, query: String): Assertion = {
    assertResult(expectedQuery, s"\n\nExpected query is:\n$expectedQuery\nActual:\n$query")(query)
  }
}
