package net.foolz.aphasia.paging

import net.foolz.aphasia.{Cols, Direction, Empty, OrderBy}
import org.scalatest.funsuite.AnyFunSuite

case class AutoIdPageToken(value: Long, operator: Operator) extends Token[Long]

class PagingQueriesTest extends AnyFunSuite  {
  test("page() when pageToken is none should return none") {
    val result = PagingQueries.page("Id", None)
    val expected = Empty
    assertResult(expected)(result)
  }

  test("page() when pagetoken is supplied should use the value") {
    val result = PagingQueries.page("Id", Some(AutoIdPageToken(100, GreaterThanEqual)))
    val expected = Cols("Id", ">=", Some(100))
    assertResult(expected)(result)
  }

  test("orderByPageToken() when page token is none should return none") {
    val result = PagingQueries.orderByPageToken(new StringBuilder, "someColumn", None)
    val expected = Empty
    assertResult(expected)(result)
  }

  test("orderByPageToken() when page token is supplied should give a query group for it") {
    val result = PagingQueries.orderByPageToken(new StringBuilder, "Id", Some(AutoIdPageToken(100, GreaterThan)))
    val expected = OrderBy(s"Id", Direction.Asc)
    assertResult(expected)(result)
  }

  test("reverseIfLessThan() when page token operator is less than reverses list") {
    val result = PagingQueries.reverseIfLessThan(Seq("a", "b", "c"), Some(AutoIdPageToken(100, LessThan)))
    val expected = Seq("c", "b", "a")
    assertResult(expected)(result)
  }

  test("reverseIfLessThan() when page token operator is greater than doesn't reverse list") {
    val result = PagingQueries.reverseIfLessThan(Seq("a", "b", "c"), Some(AutoIdPageToken(100, GreaterThan)))
    val expected = Seq("a", "b", "c")
    assertResult(expected)(result)
  }

  test("reverseIfLessThan() when page token is None doesn't reverse list") {
    val result = PagingQueries.reverseIfLessThan(Seq("a", "b", "c"), None)
    val expected = Seq("a", "b", "c")
    assertResult(expected)(result)
  }
}
