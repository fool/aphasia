val scala3Version = "3.1.0"

val scala213Version = "2.13.6"

val scala212Version = "2.12.14"

val scala211Version = "2.11.12"

lazy val `aphasia` = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "net.foolz",
      scalaVersion := scala3Version,
      version      := "0.1.0", /* not used: the version is kept in version.sbt */
      crossScalaVersions := Seq(scala3Version, scala213Version, scala212Version, scala211Version)
    )),
    name := "aphasia",
    licenses += ("MIT", url("http://opensource.org/licenses/MIT")),
    libraryDependencies += "org.scalatest"          %% "scalatest"           % "3.2.10"   % Test,
  )
