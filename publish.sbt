ThisBuild / organization := "net.foolz"
ThisBuild / organizationName := "aphasia"
ThisBuild / organizationHomepage := Some(url("https://foolz.net/"))

ThisBuild / sonatypeProfileName := "net.foolz"

ThisBuild / scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/fool/aphasia"),
    "git clone git@bitbucket.org:fool/aphasia.git"
    )
  )
ThisBuild / developers := List(
  Developer(
    id    = "foolfoolz",
    name  = "foolfoolz",
    email = "fool@foolz.net",
    url   = url("https://foolz.net")
    )
  )

ThisBuild / description := "Query writing library"
ThisBuild / licenses := List("MIT" -> new URL("https://en.wikipedia.org/wiki/MIT_License"))
ThisBuild / homepage := Some(url("https://bitbucket.org/fool/aphasia"))

ThisBuild / pomIncludeRepository := { _ => false }

ThisBuild / publishTo := sonatypePublishToBundle.value


ThisBuild / publishMavenStyle := true